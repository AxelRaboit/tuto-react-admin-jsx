import * as React from "react";
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import { Title } from 'react-admin';
import brandLogo from './assets/brand-logo.svg';

export default () => (
    <Card>
        <Title title="Not Found" />
        <CardContent>
            <div className="notFoundPage">
                <img className="brandLogo" src={brandLogo} alt="logo" />
                <h1>404: <span>Page not found</span></h1>
            </div>
        </CardContent>
    </Card>
);