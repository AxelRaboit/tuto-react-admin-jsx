import { SavedQueriesList, FilterLiveSearch, FilterList, FilterListItem } from 'react-admin';
import { Card, CardContent } from '@mui/material';
import TitleIcon from '@mui/icons-material/Title';

export const PostFilterSidebar = () => (
    <Card sx={{ order: -1, mr: 2, mt: 8, width: "auto" }}>
        <CardContent>
            <SavedQueriesList />
            <FilterLiveSearch />
            <FilterList label="Title" icon={<TitleIcon />}>
                <FilterListItem label="qui est esse" value={{ title: 'qui est esse' }} />
                <FilterListItem label="eum et est occaecati" value={{ title: 'eum et est occaecati' }} />
            </FilterList>
        </CardContent>
    </Card>
);