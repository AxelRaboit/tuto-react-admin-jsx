import React from 'react';
import { ToggleThemeButton, AppBar, defaultTheme } from 'react-admin';
import red from '@mui/material/colors/red';
import { Typography } from '@mui/material';
/* import MyLogoutButton from './MyLogoutButton'; */

const darkTheme = {
    palette: {
        mode: 'dark',
        primary: {
            main: '#41ABA1',
        },
        secondary: {
            main: '#41ABA1',
        },
        error: red,
        contrastThreshold: 3,
        tonalOffset: 0.2,
    },
};

export const lightTheme = {
    /* ...defaultTheme, */
    mode: 'light',
    palette: {
        primary: {
            main: '#41ABA1',
            light: '#757ce8',
            dark: '#002884',
            contrastText: '#fff',
        },
        secondary: {
            main: '#41ABA1',
            light: '#757ce8',
            dark: '#002884',
            contrastText: '#fff',
        },
        error: red,
        contrastThreshold: 3,
        tonalOffset: 0.2,
    },
    typography: {
        fontFamily: ['-apple-system', 'BlinkMacSystemFont', '"Segoe UI"', 'Arial', 'sans-serif'].join(','),
    },
};

/* const MyUserMenu = () => <MyLogoutButton />; */

export const MyAppBar = (props) => (
    <AppBar {...props} /* userMenu={<MyUserMenu />} */>
        <Typography flex="1" variant="h6" id="react-admin-title"></Typography>
        <ToggleThemeButton
            lightTheme={lightTheme}
            darkTheme={darkTheme}
        />
    </AppBar>
);