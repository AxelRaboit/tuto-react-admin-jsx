import { Admin, Resource, Layout } from "react-admin";
import { PostList, PostEdit, PostCreate, PostShow } from "./posts";
import { PhotoList, PhotoEdit, PhotoCreate, PhotoShow } from "./photos";
import { AlbumList } from "./albums";
import { UserList } from "./users";
import jsonServerProvider from "ra-data-json-server";
import PostIcon from "@mui/icons-material/Book";
import { Dashboard } from './Dashboard';
import { authProvider } from './authProvider';
/* import { dataProvider } from './dataProvider'; */
import { MyAppBar, lightTheme } from './MyAppBar';
import { MySidebar } from './MySidebar';
import { MyMenu } from './MyMenu';
import NotFound from './NotFound';
import MyLoginPage from './MyLoginPage';

const dataProvider = jsonServerProvider("https://jsonplaceholder.typicode.com");
// Fast menu
/* const MyLayout = (props) => <Layout {...props} appBar={MyAppBar} />; */

// Custom menu
const MyLayout = props => <Layout
    {...props}
    appBar={MyAppBar}
    sidebar={MySidebar}
    menu={MyMenu}
/>;

const App = () => (
  <Admin theme={lightTheme} loginPage={MyLoginPage} catchAll={NotFound} authProvider={authProvider} dataProvider={dataProvider} dashboard={Dashboard} layout={MyLayout}>
  {/* <Admin authProvider={authProvider} dataProvider={dataProvider} dashboard={Dashboard}> */}
    <Resource name="posts" list={PostList} /* icon={PostIcon} */ edit={PostEdit} create={PostCreate} show={PostShow} />
    <Resource name="users" list={UserList} /* icon={PeopleIcon} */ recordRepresentation="name" />
    <Resource name="albums" list={AlbumList} /* icon={AlbumIcon} */ recordRepresentation="title" />
    <Resource name="photos" list={PhotoList} /* icon={InsertPhotoIcon} */ edit={PhotoEdit} create={PhotoCreate} show={PhotoShow} />
  </Admin>
);

export default App;