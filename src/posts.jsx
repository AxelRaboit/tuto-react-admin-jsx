import {
    List,
    Datagrid,
    TextField,
    ReferenceField,
    EditButton,
    ShowButton,
    DeleteButton,
    Edit,
    DateField,
    Create,
    SimpleForm,
    SimpleShowLayout,
    Show,
    ReferenceInput,
    TextInput,
  } from "react-admin";
  import { useRecordContext} from "react-admin";
  import { PostFilterSidebar } from "./PostFilterSidebar";

/* const postFilters = [
    <TextInput source="q" label="Search" alwaysOn />,
    <ReferenceInput source="userId" label="User" reference="users" />,
]; */

const PostTitle = () => {
    const record = useRecordContext();
    return <span>Post {record ? `"${record.title}"` : ''}</span>;
};

export const PostList = () => (
    <List /* filters={postFilters} */ aside={<PostFilterSidebar />}>
      <Datagrid>
        <TextField source="id" />
        <ReferenceField source="userId" reference="users" />
        <TextField source="title" />
        <ShowButton />
        <EditButton />
        <DeleteButton />
      </Datagrid>
    </List>
);

export const PostEdit = () => (
    <Edit title={<PostTitle />}>
      <SimpleForm>
        <TextInput source="id" disabled />
        <ReferenceInput source="userId" reference="users" />
        <TextInput source="title" />
        <TextInput source="body" multiline rows={5} />
      </SimpleForm>
    </Edit>
);

export const PostShow = () => (
    <Show title={<PostTitle />}>
      <SimpleShowLayout>
        <TextField source="id" />
        <ReferenceField source="userId" reference="users" />  
        <TextField source="title" />
        <TextField source="body" />
      </SimpleShowLayout>
    </Show>
);

export const PostCreate = () => (
    <Create>
        <SimpleForm>
            <ReferenceInput source="userId" reference="users" />
            <TextInput source="title" />
            <TextInput source="body" multiline rows={5} />
        </SimpleForm>
    </Create>
);