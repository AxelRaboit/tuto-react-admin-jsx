import * as React from 'react';
import { useState } from 'react';
import { useLogin, useNotify, Notification } from 'react-admin';
import brandLogo from './assets/brand-logo.svg';

const MyLoginPage = ({ theme }) => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const login = useLogin();
    const notify = useNotify();

    const handleSubmit = e => {
        e.preventDefault();
        login({ email, password }).catch(() =>
            notify('Invalid email or password')
        );
    };

    return (
        <div className="loginPage">
            <form className="loginForm" onSubmit={handleSubmit}>
                <div className="loginFormContainer">
                    <img className="brandLogo" src={brandLogo} alt="logo" />
                    <input
                        name="email"
                        type="email"
                        value={email}
                        onChange={e => setEmail(e.target.value)}
                    />
                    <input
                        name="password"
                        type="password"
                        value={password}
                        onChange={e => setPassword(e.target.value)}
                    />
                    <button>Login</button>
                </div>
            </form>
        </div>
    );
};

export default MyLoginPage;