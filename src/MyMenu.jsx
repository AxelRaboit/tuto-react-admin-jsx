import * as React from 'react';
import { Menu } from 'react-admin';
import PeopleIcon from '@mui/icons-material/People';
import PostIcon from "@mui/icons-material/Book";
import AlbumIcon from '@mui/icons-material/Album';
import InsertPhotoIcon from '@mui/icons-material/InsertPhoto';
import DashboardIcon from '@mui/icons-material/Dashboard';

export const MyMenu = (props) => (
    <Menu {...props}>
        <Menu.DashboardItem leftIcon={<DashboardIcon color="primary" />}/>
        <Menu.Item to="/users" primaryText="Users" leftIcon={<PeopleIcon color="primary" />}/>
        <Menu.Item to="/posts" primaryText="Posts" leftIcon={<PostIcon color="primary" />}/>
        <Menu.Item to="/albums" primaryText="Albums" leftIcon={<AlbumIcon color="primary" />}/>
        <Menu.Item to="/photos" primaryText="Photos" leftIcon={<InsertPhotoIcon color="primary" />}/>
    </Menu>
);