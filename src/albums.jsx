import { useMediaQuery } from "@mui/material";
import { List, SimpleList, Datagrid, TextField, TextInput, ReferenceInput } from "react-admin";

const albumFilters = [
  <TextInput source="q" label="Search" alwaysOn />,
  <ReferenceInput source="userId" label="Album" reference="users" />,
];

export const AlbumList = () => {
  const isSmall = useMediaQuery((theme) => theme.breakpoints.down("sm"));
  return (
    <List filters={albumFilters}>
      {isSmall ? (
        <SimpleList
          primaryText={(record) => record.title}
        />
      ) : (
        <Datagrid rowClick="edit">
          <TextField source="id" />
          <TextField source="title" />
          <TextField source="userId" />
        </Datagrid>
      )}
    </List>
  );
};