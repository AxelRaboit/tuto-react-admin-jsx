import {
    List,
    Datagrid,
    TextField,
    ReferenceField,
    EditButton,
    ShowButton,
    DeleteButton,
    Edit,
    DateField,
    ImageField,
    Create,
    SimpleForm,
    SimpleShowLayout,
    Show,
    ReferenceInput,
    TextInput,
  } from "react-admin";
  import { useRecordContext} from "react-admin";

const photoFilters = [
    <TextInput source="q" label="Search" alwaysOn />,
    <ReferenceInput source="albumId" label="Album" reference="albums" />,
];

const PhotoTitle = () => {
    const record = useRecordContext();
    return <span>Photo {record ? `"${record.title}"` : ''}</span>;
};

export const PhotoList = () => (
    <List filters={photoFilters}>
      <Datagrid>
        <TextField source="id" />
        <ReferenceField source="albumId" reference="albums" />
        <TextField source="title" />
        <TextField source="url" />
        <ImageField source="thumbnailUrl" sx={{ '& img': { maxWidth: 50, maxHeight: 50, objectFit: 'contain' } }}/>
        <ShowButton />
        <EditButton />
        <DeleteButton />
      </Datagrid>
    </List>
);

export const PhotoEdit = () => (
    <Edit title={<PhotoTitle />}>
      <SimpleForm>
        <TextInput source="id" disabled />
        <ReferenceInput source="albumId" reference="albums" />
        <TextInput source="title" />
        <TextInput source="url" />
        <TextInput source="thumbnailUrl" />
      </SimpleForm>
    </Edit>
);

export const PhotoShow = () => (
    <Show title={<PhotoTitle />}>
      <SimpleShowLayout>
        <TextField source="id" />
        <ReferenceField source="albumId" reference="albums" />  
        <TextField source="title" />
        <TextField source="url" />
        <TextField source="thumbnailUrl" />
      </SimpleShowLayout>
    </Show>
);

export const PhotoCreate = () => (
    <Create>
        <SimpleForm>
            <ReferenceInput source="albumId" reference="albums" />
            <TextInput source="title" />
            <TextInput source="url" />
            <TextInput source="thumbnailUrl" />
        </SimpleForm>
    </Create>
);